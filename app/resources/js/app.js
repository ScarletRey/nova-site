$(function() {

	if (document.documentMode || /Edge/.test(navigator.userAgent)) {
	    $('.cool-pic').hide();
	}

    $('.main-gallery').owlCarousel({
	    loop:true,
	    margin: 30,
	    nav: 1,
	    dots: 0,
	    navText: [,],
	    responsive:{
	        0:{
	            items:1,
	            autoplay: 1,
	        },
	        480:{
	            items:2,
	        },
	        767:{
	            items:3,
	        }
	    }
	})

	function renderLines() {
		setTimeout(function() {
			var a = $('.main-revievs-item.positive').data('count'),
				b = $('.main-revievs-item.neutral').data('count'),
				c = $('.main-revievs-item.negative').data('count'),
				sum = a + b + c;
			a = Math.round(a / sum * 100);
			b = Math.round(b / sum * 100);
			c = Math.round(c / sum * 100);
			$('.main-revievs-item.positive .line').css('width', a + "%")
			$('.main-revievs-item.neutral .line').css('width', b + "%")
			$('.main-revievs-item.negative .line').css('width', c + "%")
		}, 2500);
	}

	renderLines();

	$('.main-video-thumb').on('click', function () {
		var src = $('#yt-video').data('src')
		$(this).removeClass('active');
		$('#yt-video').attr('src',src);
	})

});