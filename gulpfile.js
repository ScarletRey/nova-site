var autoprefixer = require('gulp-autoprefixer'), //Автопрефиксер
    browserSync  = require('browser-sync'); // Локальный сервер
    cleanCSS     = require('gulp-clean-css'), // Минификация css
    gulp         = require('gulp'), // Сообственно Gulp JS
    jade         = require('gulp-jade'); // Jade
    plumber      = require('gulp-plumber'); // Отслеживаем ошибки
    rupture      = require('rupture');
    stylus       = require('gulp-stylus'); // Stylus
    watch        = require('gulp-watch'); // Вотчер

// Директории
var path = {

    app: {
        jade:      'app/pages/*.jade',
        styl:      'app/styles/main.styl',
        resources: 'app/resources/**/*'
    },

    watch: {
        jade: 'app/{blocks,pages}/**/*.jade',
        styl: 'app/{blocks,styles}/**/*.styl',
        resources: 'app/resources/**/*'
    }
};

// Компиляция Jade
gulp.task('templates', function() {
  gulp.src(path.app.jade)
        .pipe(plumber())
        .pipe(jade({ pretty: true }))
        .pipe(gulp.dest('dist'));
});

// Компиляция стилей
gulp.task('styles', function() {
    gulp.src(path.app.styl)
        .pipe(plumber())
        .pipe(stylus({
            use: [
                rupture()
            ]
        }))
        .pipe(autoprefixer({
            browsers: ['last 5 versions'],
        }))
        .pipe(cleanCSS({
            format: 'keep-breaks'
        }))
        .pipe(gulp.dest('dist/css'));
});

// Копирование ресурсов
gulp.task('copy', function() {
    gulp.src(path.app.resources)
    .pipe(gulp.dest('dist'))
})

// Таск на вотчер
gulp.task('watch', function(){

	browserSync.init({
            files: ['dist/**/*'],
    		server: {
    			baseDir: "dist"
    		},
            host: 'localhost',
            port: 3000
	});

    gulp.watch( path.watch.jade, ['templates']   );
    gulp.watch( path.watch.styl, ['styles']   );
    gulp.watch( path.watch.resources, ['copy']   );

    // watch([path.app.jade], function(event, cb) {
    //     gulp.start('templates');
    // });

	// watch("dist/*.html").on('change', browserSync.reload);
});



gulp.task('default', [
  'templates',
  'styles',
  'copy',
  'watch'
]);

gulp.task('server', function(){
    browserSync.init({
        server: {
            baseDir: "test_server"
        }
    });
    watch("test_server/*.*").on('change', browserSync.reload);
    watch("test_server/**/*.*").on('change', browserSync.reload);
});

gulp.task('test', [
  'server'
]);
